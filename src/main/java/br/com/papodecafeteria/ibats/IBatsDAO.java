package br.com.papodecafeteria.ibats;

import java.io.Reader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import br.com.papodecafeteria.domain.User4J;

public class IBatsDAO {
	
	private static Logger l = Logger.getLogger(IBatsDAO.class.getName());
	
	public static User4J getRecordById(int pId, SqlMapClient pSqlMapClient){
		User4J pUser4J = null;
		try {
			pUser4J = (User4J) pSqlMapClient.queryForObject("User4J.getById", pId);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pUser4J;
	}
	
	public static List<User4J> getAllRecords(SqlMapClient pSqlMapClient){
		List<User4J> lstUser4J = null;
		try {
			lstUser4J = (List<User4J>) pSqlMapClient.queryForList("User4J.getAllRecords", null);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return lstUser4J;
	}
	
	public static void delete(User4J pUser4J, SqlMapClient pSqlMapClient){
		try{
			pSqlMapClient.delete("User4J.delete", pUser4J.getiId());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void update(User4J pUser4J, SqlMapClient pSqlMapClient){
		try{
			pSqlMapClient.update("User4J.update", pUser4J);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static int insert(User4J pUser4J, SqlMapClient pSqlMapClient){
		int iStatus = 0;
		try{
			pSqlMapClient.insert("User4J.insert", pUser4J);
			iStatus = pUser4J.getiId();
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return iStatus;
	}
}
