package br.com.papodecafeteria.domain;

import java.util.logging.Level;
import java.util.logging.Logger;

public class User4J {
	private static Logger l = Logger.getLogger(User4J.class.getName());
	
	private int 	iId = 0;
	private String 	cName = "";
	private String 	cPassword = "";
	private String 	cSex = "";
	private String 	cEmail = "";
	private String 	ccountry= "";
	
	public User4J(){
		super();
	}
	
	public User4J(int pId, String pName, String pPassword, String pSex, String pEmail, String pCountry){
		setiId(pId);
		setcName(pName);
		setcPassword(pPassword);
		setcSex(pSex);
		setcEmail(pEmail);
		setcCountry(pCountry);
	}
	
	public int getiId() {
		return iId;
	}

	public void setiId(int iId) {
		this.iId = iId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcPassword() {
		return cPassword;
	}

	public void setcPassword(String cPassword) {
		this.cPassword = cPassword;
	}

	public String getcSex() {
		return cSex;
	}

	public void setcSex(String cSex) {
		this.cSex = cSex;
	}

	public String getcEmail() {
		return cEmail;
	}

	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}

	public String getcCountry() {
		return ccountry;
	}

	public void setcCountry(String ccountry) {
		this.ccountry = ccountry;
	}

	@Override
	public String toString(){
		String cReturn = "";
		try{
			
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
}
