package br.com.papodecafeteria;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;

import com.ibatis.sqlmap.client.SqlMapClient;

import br.com.papodecafeteria.dao.Dao4User4J;
import br.com.papodecafeteria.domain.User4J;
import br.com.papodecafeteria.domain.User4JPA;
import br.com.papodecafeteria.ibats.IBatsDAO;
import br.com.papodecafeteria.jpa.Actions;
import br.com.papodecafeteria.utilities.IBatsUtil;
import br.com.papodecafeteria.utilities.JDBCUtil;
import br.com.papodecafeteria.utilities.JPAHibernateUtil;

public class App {
	private static Logger l = Logger.getLogger(App.class.getName());
	
    public static void main( String[] args ){
        System.out.println( "JDBC X iBats X JPA Hibernate with Annotations\n\n" );
        
        User4J pOldUser = getFilledUserInstance();
        User4J pNewUser = getFilledUpdatedUserInstance();
        
        Connection connJDBC = null;
        SqlMapClient iBatsSqlMapClient = null;
        Session jpaSession = null;
        
        try{
        	Instant t = null;
        	
        	t = Instant.now();
	        connJDBC = JDBCUtil.getConnection();
    		System.out.println("\tSetup JDBC: " + Duration.between(t, Instant.now()).toMillis() + " ms");
        	
        	t = Instant.now();
        	iBatsSqlMapClient = IBatsUtil.getSqlMapClient();
    		System.out.println("\tSetup iBats: " + Duration.between(t, Instant.now()).toMillis() + " ms");
        	
        	t = Instant.now();
	        jpaSession = JPAHibernateUtil.getSessionFactory().openSession();
    		System.out.println("\tSetup JPA Hibernate with Annotations: " + Duration.between(t, Instant.now()).toMillis() + " ms");
	        
	        execJDBC(pOldUser, pNewUser, connJDBC);
	        execIBats(pOldUser, pNewUser, iBatsSqlMapClient);
	        execJPA4Hibernate(pOldUser, pNewUser, jpaSession);
        }catch (Exception exc) {
        	l.log(Level.SEVERE, exc.getMessage(), exc);
		}finally {
			try {
				if(!connJDBC.isClosed())
					connJDBC.close();
			} catch (SQLException e) {
				l.log(Level.SEVERE, e.getMessage());
			}
	        connJDBC = null;
	        
	        iBatsSqlMapClient = null;
	        
	        if(jpaSession.isOpen())
	        	jpaSession.close();
	        jpaSession = null;
		}
        

    }
    
    private static User4J getFilledUserInstance(){
    	User4J user = null;
    	try{
    		user = new User4J(0, "Fulano de Tal", "password", "male", "email@email.com.br", "Chile");
    	}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    	return user;
    }
    
    private static User4J getFilledUpdatedUserInstance(){
    	User4J user = null;
    	try{
    		user = new User4J(0, "Fulano de Tal Updated", "password", "male", "email@email.updated.com.br", "Argentina");
    	}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    	return user;
    }
    
    private static void execJPA4Hibernate(User4J pOldUser, User4J pNewUser, Session pSession){
    	Instant t0 = null;
    	Instant t1 = null;
    	Instant t2 = null;
    	
    	User4JPA pOldUser4JPA = new User4JPA(
    			pOldUser.getiId(), 
    			pOldUser.getcName(), 
    			pOldUser.getcPassword(), 
    			pOldUser.getcEmail(), pOldUser.getcSex(), pOldUser.getcCountry());
    	
    	User4JPA pNewUser4JPA = new User4JPA(
    			pNewUser.getiId(), 
    			pNewUser.getcName(), 
    			pNewUser.getcPassword(), 
    			pNewUser.getcEmail(), pNewUser.getcSex(), pNewUser.getcCountry());
    	;
    	
    	System.out.println("\n\n\tExecuting JPA Hibernate with Annotations...\n");
    	
    	try{
    		t0 = Instant.now();
    		t1 = t0;
    		List<User4JPA> lstUser4JPA = Actions.getAllRecords(pSession);
    		t2 = Instant.now();
    		
    		System.out.println("\tActions.getAllRecords() - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		t1 = Instant.now();
    		pOldUser4JPA = Actions.insert(pOldUser4JPA, pSession);
    		t2 = Instant.now();
    		
    		System.out.println("\tActions.insert(pOldUser4JPA) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		t1 = Instant.now();
    		pOldUser4JPA = Actions.getRecordById(pOldUser4JPA, pSession);
    		t2 = Instant.now();
    		
    		System.out.println("\tActions.getRecordById(pOldUser4JPA) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		pNewUser4JPA.setiId(pOldUser4JPA.getiId());
    		t1 = Instant.now();
    		pNewUser4JPA = Actions.update(pNewUser4JPA, pSession);
    		t2 = Instant.now();
    		
    		System.out.println("\tActions.update(pNewUser4JPA) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");

    		t1 = Instant.now();
    		Actions.delete(pNewUser4JPA, pSession);
    		t2 = Instant.now();
    		
    		System.out.println("\tActions.delete(pNewUser4JPA) - Fim: Tempo total de execu��o de " + Duration.between(t1, t2).toMillis() + " ms");
    		System.out.println("\n\texecJPA4Hibernate - Fim: Tempo total de execu��o de " + Duration.between(t0, t2).toMillis() + " ms");

    		//Util.printListUserJPA(Dao4User4J.getAllRecords());
    		
    	}catch (Exception exc) {
    		l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    }
    
    private static void execIBats(User4J pOldUser, User4J pNewUser, SqlMapClient pSqlMapClient){
    	Instant t0 = null;
    	Instant t1 = null;
    	Instant t2 = null;
    	
    	int iStatus = 0;
    	
    	System.out.println("\n\n\tExecuting iBats...\n");
    	
    	try{
    		t0 = Instant.now();
    		t1 = t0;
    		List<User4J> lstUser4J = IBatsDAO.getAllRecords(pSqlMapClient);
    		t2 = Instant.now();
    		
    		System.out.println("\tIBatsDAO.getAllRecords() - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		t1 = Instant.now();
    		iStatus = IBatsDAO.insert(pOldUser, pSqlMapClient);
    		pOldUser.setiId(iStatus);
    		t2 = Instant.now();
    		
    		System.out.println("\tIBatsDAO.insert(pOldUser) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		t1 = Instant.now();
    		pOldUser = IBatsDAO.getRecordById(iStatus, pSqlMapClient);
    		t2 = Instant.now();
    		
    		System.out.println("\tIBatsDAO.getRecordById(iStatus) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		pNewUser.setiId(iStatus);
    		t1 = Instant.now();
    		IBatsDAO.update(pNewUser, pSqlMapClient);
    		pOldUser = IBatsDAO.getRecordById(iStatus, pSqlMapClient);
    		t2 = Instant.now();
    		
    		System.out.println("\tIBatsDAO.update(pNewUser) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		pNewUser.setiId(iStatus);
    		t1 = Instant.now();
    		IBatsDAO.delete(pNewUser, pSqlMapClient);
    		t2 = Instant.now();
    		
    		System.out.println("\tIBatsDAO.delete(pNewUser) - Fim: Tempo total de execu��o de " + Duration.between(t1, t2).toMillis() + " ms");
    		System.out.println("\n\texecIBats - Fim: Tempo total de execu��o de " + Duration.between(t0, t2).toMillis() + " ms");

    		//Util.printListUserJPA(Dao4User4J.getAllRecords());
    		
    	}catch (Exception exc) {
    		l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    }
    
    private static void execJDBC(User4J pOldUser, User4J pNewUser, Connection pConn){
    	Instant t0 = null;
    	Instant t1 = null;
    	Instant t2 = null;
    	
    	int iStatus = 0;
    	
    	System.out.println("\tExecuting JDBC...\n");
    	
    	try{
    		t0 = Instant.now();
    		t1 = t0;
    		List<User4J> lstUser4J = Dao4User4J.getAllRecords(pConn);
    		t2 = Instant.now();
    		
    		System.out.println("\tDao4User4J.getAllRecords() - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		t1 = Instant.now();
    		iStatus = Dao4User4J.insert(pOldUser, pConn);
    		pOldUser.setiId(iStatus);
    		t2 = Instant.now();
    		
    		System.out.println("\tDao4User4J.insert(pOldUser) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		t1 = Instant.now();
    		pOldUser = Dao4User4J.getRecordById(iStatus, pConn);
    		t2 = Instant.now();
    		
    		System.out.println("\tDao4User4J.getRecordById(iStatus) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		pNewUser.setiId(iStatus);
    		t1 = Instant.now();
    		Dao4User4J.update(pNewUser, pConn);
    		pOldUser = Dao4User4J.getRecordById(iStatus, pConn);
    		t2 = Instant.now();
    		
    		System.out.println("\tDao4User4J.update(pNewUser) - Fim: Tempo total de execu��o de " + Duration.between(t0, t1).toMillis() + " ms");
    		
    		pNewUser.setiId(iStatus);
    		t1 = Instant.now();
    		Dao4User4J.delete(pNewUser, pConn);
    		t2 = Instant.now();
    		
    		System.out.println("\tDao4User4J.delete(pNewUser) - Fim: Tempo total de execu��o de " + Duration.between(t1, t2).toMillis() + " ms");
    		System.out.println("\n\texecJDBC - Fim: Tempo total de execu��o de " + Duration.between(t0, t2).toMillis() + " ms");

    		//Util.printListUserJPA(Dao4User4J.getAllRecords());
    		
    	}catch (Exception exc) {
    		l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    }
}
