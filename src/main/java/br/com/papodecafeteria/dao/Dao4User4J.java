package br.com.papodecafeteria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.domain.User4J;

public class Dao4User4J {
	
	private static Logger l = Logger.getLogger(Dao4User4J.class.getName());
	
	public static int insert(User4J pUser, Connection pConn){  
	    int status = 0;  
	    
	    try{
	        PreparedStatement ps = pConn.prepareStatement(  
	        	"insert into user(name, password, email, sex, country) values(?,?,?,?,?);", Statement.RETURN_GENERATED_KEYS);  
	        
	        ps.setString(1,pUser.getcName());  
	        ps.setString(2,pUser.getcPassword());  
	        ps.setString(3,pUser.getcEmail());  
	        ps.setString(4,pUser.getcSex());  
	        ps.setString(5,pUser.getcCountry());
	        
	        status = ps.executeUpdate();
	        if(status > 0){
		        ResultSet pResultSet = ps.getGeneratedKeys();
	            if (pResultSet.next())
	            	status = pResultSet.getInt(1);
	        }
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return status;  
	}
	
	public static int update(User4J pUser, Connection pConn){  
	    int status = 0;  
	    try{
	        PreparedStatement ps = pConn.prepareStatement(  
	        	"update user set name = ?, email = ?, sex = ?, country = ? where id = ?;");
	        
	        ps.setString(1, pUser.getcName());
	        ps.setString(2, pUser.getcEmail());  
	        ps.setString(3, pUser.getcSex());  
	        ps.setString(4, pUser.getcCountry());  
	        ps.setInt(5, pUser.getiId());  
	        
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return status;  
	} 
	
	public static int delete(User4J pUser, Connection pConn){  
	    int status = 0;  
	    
	    try{
	        PreparedStatement ps = pConn.prepareStatement("delete from user where id = ?;");

	        ps.setInt(1, pUser.getiId());  
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	  
	    return status;  
	} 
	
	public static List<User4J> getAllRecords(Connection pConn){  
	    List<User4J> lstUser = null;  
	    try{
	        PreparedStatement ps = pConn.prepareStatement("select id, name, email, sex, country from user;");  
	        ResultSet rs = ps.executeQuery(); 
	        if(rs.isBeforeFirst())
	        	lstUser = new ArrayList<User4J>();
	        while(rs.next())
	        	lstUser.add(new User4J(rs.getInt("id"), rs.getString("name"), "", rs.getString("sex"), rs.getString("email"), rs.getString("country")));
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return lstUser;
	}

	public static User4J getRecordById(int pId, Connection pConn){  
		User4J user = null;  
	    
	    try{
	        PreparedStatement ps = pConn.prepareStatement(
	        	"select id, name, email, sex, country from user where id=?;");  
	        ps.setInt(1, pId);  
	        ResultSet rs = ps.executeQuery();  
	        
	        if(rs.isBeforeFirst()){
	        	rs.next();
	        	user = new User4J(rs.getInt("id"), rs.getString("name"), "", rs.getString("sex"), rs.getString("email"), rs.getString("country"));
	        }

	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return user;  
	}
}
