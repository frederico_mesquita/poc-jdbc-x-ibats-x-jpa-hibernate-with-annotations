package br.com.papodecafeteria.jpa;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.papodecafeteria.domain.User4JPA;
import br.com.papodecafeteria.utilities.Util;

public class Core {
	private static Logger l = Logger.getLogger(Core.class.getName());

	public static Object exec(String pAction, Object pObj, Session pSession){
        Transaction transaction = null;
		Object obj = null;
		try {
			if(pAction.equalsIgnoreCase("getById")){
				obj = (Object) pSession.find( User4JPA.class, ((User4JPA) pObj).getiId());
			} else if(pAction.equalsIgnoreCase("getAllRecords")){
			      List<User4JPA> lstUser4JPA = (List<User4JPA>) pSession.createQuery("FROM User4JPA").list();
			      obj = (Object) lstUser4JPA;
			} else {
				transaction = pSession.beginTransaction();
				if(pAction.equalsIgnoreCase("insert")){
					pSession.save((User4JPA) pObj);
					obj = pObj;
				} else if(pAction.equalsIgnoreCase("update")){
					User4JPA user4JPA = (User4JPA) pObj;
					User4JPA user4JPA2Update = pSession.find( User4JPA.class, user4JPA.getiId() );
					obj = (Object) Util.mergeUserJPA2Update(user4JPA2Update, user4JPA);
					pSession.update(user4JPA2Update);
				} else if(pAction.equalsIgnoreCase("delete")){
					pSession.delete((User4JPA) pObj);
				}
				transaction.commit();
			}
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
			if(null != transaction && transaction.isActive())
				transaction.rollback();
		}
		return obj;
	}
}
