package br.com.papodecafeteria.jpa;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;

import br.com.papodecafeteria.domain.User4JPA;

public class Actions {
	private static Logger l = Logger.getLogger(Actions.class.getName());
	
	public static List<User4JPA> getAllRecords(Session pSession){
		List<User4JPA> lstUser4JPA = null;
		try {
			lstUser4JPA = (List<User4JPA>) Core.exec("getAllRecords", null, pSession);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return lstUser4JPA;
	}
	
	public static User4JPA getRecordById(User4JPA pUser4JPA, Session pSession){
		User4JPA user4JPA = null;
		try {
			user4JPA = (User4JPA) Core.exec("getById", (Object) pUser4JPA, pSession);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return user4JPA;
	}
	
	public static void delete(User4JPA pUser4JPA, Session pSession){
		try {
			Core.exec("delete", (Object) pUser4JPA, pSession);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static User4JPA update(User4JPA pUser4JPA, Session pSession){
		User4JPA user4JPA = null;
		try {
			user4JPA = (User4JPA) Core.exec("update", (Object) pUser4JPA, pSession);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return user4JPA;
	}
	
	public static User4JPA insert(User4JPA pUser4JPA, Session pSession){
		User4JPA user4JPA = null;
		try {
			user4JPA = (User4JPA) Core.exec("insert", (Object) pUser4JPA, pSession);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return user4JPA;
	}
}
