package br.com.papodecafeteria.utilities;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCUtil {
	
	private static Logger l = Logger.getLogger(JDBCUtil.class.getName());
	
	protected static Properties loadProperties(String pReference){
		Properties properties = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(pReference);
			properties = new Properties();
			properties.load(fileInputStream);
			fileInputStream = null;
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return properties;
	}

	public static Connection getConnection(){  
	    Connection conn = null;
	    try{  
	    	ClassLoader loader = Thread.currentThread().getContextClassLoader();
	    	
	    	
	    	//Properties properties = loadProperties(UserDaoJpa.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "/connection.property");
	    	Properties properties = new Properties();
	    	properties.load(loader.getResourceAsStream("connection.property"));
	        Class.forName(properties.getProperty("dbclass"));	        
	        String dburl = properties.getProperty("dbjdbctype") + "://" + properties.getProperty("dbaddress") + "/" + 
        					properties.getProperty("dbinstance") + "?useSSL=" + properties.getProperty("dbusessl") + 
        					"&useTimezone=" + properties.getProperty("dbusetimezone") +
        					"&serverTimezone=" + properties.getProperty("dbservertimezone"); 
	        
	        conn = DriverManager.getConnection(dburl, properties.getProperty("userdb"), properties.getProperty("userdbpasswd"));
	        properties = null;
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return conn;  
	}
}
