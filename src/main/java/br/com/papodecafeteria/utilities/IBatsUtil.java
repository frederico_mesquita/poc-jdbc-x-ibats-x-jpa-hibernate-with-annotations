package br.com.papodecafeteria.utilities;

import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class IBatsUtil {
	
	private static Logger l = Logger.getLogger(IBatsUtil.class.getName());
	
	public static SqlMapClient getSqlMapClient(){
		SqlMapClient smc = null;
		try {
			Reader rd = Resources.getResourceAsReader("SqlMapConfig.xml");
			smc = SqlMapClientBuilder.buildSqlMapClient(rd);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return smc;
	}
}
