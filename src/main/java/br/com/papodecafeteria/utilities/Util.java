package br.com.papodecafeteria.utilities;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.domain.User4J;
import br.com.papodecafeteria.domain.User4JPA;

public class Util {
	private static Logger l = Logger.getLogger(Util.class.getName());
	
	public static User4JPA mergeUserJPA2Update(User4JPA pOld, User4JPA pNew){
		User4JPA pUser4JPA = pOld;
		try { 
			pUser4JPA.setcName(pNew.getcName());
			pUser4JPA.setcEmail(pNew.getcEmail());
			pUser4JPA.setcSex(pNew.getcSex());
			pUser4JPA.setcCountry(pNew.getcCountry());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pUser4JPA;
	}
	
	public static void printUserJPA(User4J pUser4J){
		try {
			System.out.println("(" + pUser4J.getiId() + ") " + pUser4J.getcName());
			System.out.println("\tEmail: " + pUser4J.getcEmail());
			System.out.println("\tSex: " + pUser4J.getcSex());
			System.out.println("\tCountry:" + pUser4J.getcCountry() + "\n");
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void printListUserJPA(List<User4J> pListUser4J){
		try {
			for(User4J user4J : pListUser4J)
				printUserJPA(user4J);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
}
