	JDBC X iBats X JPA Hibernate with Annotations


	Setup JDBC: 498 ms
	Setup iBats: 208 ms
	Setup JPA Hibernate with Annotations: 1563 ms
	
	Executing JDBC...

		Dao4User4J.getAllRecords() - Fim: Tempo total de execução de 0 ms
		Dao4User4J.insert(pOldUser) - Fim: Tempo total de execução de 7 ms
		Dao4User4J.getRecordById(iStatus) - Fim: Tempo total de execução de 69 ms
		Dao4User4J.update(pNewUser) - Fim: Tempo total de execução de 85 ms
		Dao4User4J.delete(pNewUser) - Fim: Tempo total de execução de 17 ms

		execJDBC - Fim: Tempo total de execução de 169 ms

	Executing iBats...

		IBatsDAO.getAllRecords() - Fim: Tempo total de execução de 0 ms
		IBatsDAO.insert(pOldUser) - Fim: Tempo total de execução de 16 ms
		IBatsDAO.getRecordById(iStatus) - Fim: Tempo total de execução de 63 ms
		IBatsDAO.update(pNewUser) - Fim: Tempo total de execução de 63 ms
		IBatsDAO.delete(pNewUser) - Fim: Tempo total de execução de 31 ms

		execIBats - Fim: Tempo total de execução de 131 ms

	Executing JPA Hibernate with Annotations...

		Actions.getAllRecords() - Fim: Tempo total de execução de 0 ms
		Actions.insert(pOldUser4JPA) - Fim: Tempo total de execução de 217 ms
		Actions.getRecordById(pOldUser4JPA) - Fim: Tempo total de execução de 351 ms
		Actions.update(pNewUser4JPA) - Fim: Tempo total de execução de 355 ms
		Actions.delete(pNewUser4JPA) - Fim: Tempo total de execução de 31 ms

		execJPA4Hibernate - Fim: Tempo total de execução de 417 ms
